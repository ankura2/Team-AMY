################################################### Connecting to AWS
import boto3

import json
################################################### Create random name for things
import random
import string
import os

################################################### Parameters for Thing
thingArn = ''
thingId = ''
#thingName = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(5)])
deviceName = input("Please enter your device name: ")
thingName = "device_"+deviceName
defaultPolicyName = 'Stella_policy'
key_folder = 'E:/CS_Master_Degree_UIUC/CS437 Internet of Things/Lab4/keys/'+thingName
if not os.path.exists(key_folder):
    os.makedirs(key_folder)
###################################################

def createThing():
    global thingClient
    thingResponse = thingClient.create_thing(thingName = thingName)
    data = json.loads(json.dumps(thingResponse, sort_keys=False, indent=4))
    for element in data: 
        if element == 'thingArn':
            thingArn = data['thingArn']
        elif element == 'thingId':
            thingId = data['thingId']
    createCertificate()

def createCertificate():
    global thingClient
    certResponse = thingClient.create_keys_and_certificate(setAsActive = True)
    data = json.loads(json.dumps(certResponse, sort_keys=False, indent=4))
    for element in data: 
        if element == 'certificateArn':
            certificateArn = data['certificateArn']
        elif element == 'keyPair':
            PublicKey = data['keyPair']['PublicKey']
            PrivateKey = data['keyPair']['PrivateKey']
        elif element == 'certificatePem':
            certificatePem = data['certificatePem']
        elif element == 'certificateId':
            certificateId = data['certificateId']
			
    file_names = ['public.pem','private.pem','cert.pem']
    for fn in file_names:
	    file = os.path.join(key_folder, fn)
	    if fn == 'public.pem':
		    with open(file, 'w') as outfile:
			    outfile.write(PublicKey)
	    elif fn == 'private.pem':
		    with open(file, 'w') as outfile:
			    outfile.write(PrivateKey)
	    elif fn == 'cert.pem':
		    with open(key_folder+'/cert.pem', 'w') as outfile:
			    outfile.write(certificatePem)

    response = thingClient.attach_policy(policyName = defaultPolicyName,target = certificateArn)
    response = thingClient.attach_thing_principal(thingName = thingName,principal = certificateArn)


thingClient = boto3.client('iot')
createThing()
