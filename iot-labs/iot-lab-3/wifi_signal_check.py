import subprocess

def wifi_quality():
    cmd = 'iwconfig'

    out = subprocess.Popen([cmd, 'wlan0'], stdout = subprocess.PIPE)

    output = str(out.communicate())
    wifi_qual_ind = output.find("Link Quality=")
    wifi_qual = output[wifi_qual_ind: wifi_qual_ind+18]

    return wifi_qual



if __name__ == '__main__':
    wifi_qual = wifi_quality()
    print(wifi_qual)
